import java.util.LinkedList;

public class Empresinha {
    private LinkedList<Funcionario> funcionarios = new LinkedList<Funcionario>(); //Lista de funcionarios

    //Retorna a lista de funcionarios da empresa

    public LinkedList<Funcionario> getFuncionarios(){  //pega funcionarios e coloca na lista
        return this.funcionarios;
    }

    // Inclui um novo funcionario

    public void contratar(Funcionario novoFuncionario){ //"Contrata" funcionario, vai adicionar os funcionarios na lista
        this.funcionarios.add(novoFuncionario);
    }

    /**
     *  Dissidio
     */

    public void concederAumento(double percentual){
        for (Funcionario atual : this.funcionarios){   //Faz o loop para todos funcionarios
            double salario = atual.getSalario();
            double aumento = 1 + percentual;
            double novoSalario = salario * aumento;
            atual.setSalario(novoSalario);
        }
    }

    public void listarTelaDeFuncionario(){
        double salarioAnualFuncionarios=0;

        for(Funcionario atual : this.funcionarios){
            System.out.println("\nNome: " + atual.getNome() + " \nSalario: " + atual.getSalario() + " \nSalario Anual: " + atual.getSalarioAnual());
            salarioAnualFuncionarios = salarioAnualFuncionarios + atual.getSalarioAnual();
        }
        System.out.println();
        System.out.println("Soma de todos salarios anuais da empresa: " + salarioAnualFuncionarios);
    }

    public void listarFuncionarioComMaiorSalario(){
        String funcionario = "";
        double maiorSalario = 0;

        for(Funcionario atual : this.funcionarios){
            if (atual.getSalario() > maiorSalario){  // compara os salarios dos funcionarios
                maiorSalario = atual.getSalario();  //Pega o maior salario
                funcionario = atual.getNome();  //Pega o nome do individuo com maior salario
            }
        }
        System.out.println("\nNome: " + funcionario + "\nSalario: " + Double.toString(maiorSalario));  // lista nome do funcionario e seu salario
    }

    public void demitirFuncionairoComMaiorSalario(){
        String funcionario = "";   //Variaveis para fazer as comparações
        double maiorSalario = 0;
        Funcionario removerfuncionario = null;

        for (Funcionario atual : this.funcionarios){
            if (atual.getSalario() > maiorSalario){   //Compara os salarios
                maiorSalario = atual.getSalario();   //Pega o maior salario
                funcionario = atual.getNome();   //Pega o nome 
                removerfuncionario = atual;      //Pega o nome para remover
            }
        }
        this.funcionarios.remove(removerfuncionario); //Função já do java remove o funcionario

        System.out.println(funcionario + " foi despedido com sucesso");
    }

}

