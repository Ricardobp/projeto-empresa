import java.util.Scanner;

public class TelaEmpresa {
    Empresinha empresinha = new Empresinha();

    public void mostrarTelaIncluirFuncionario(){
        Scanner teclado = new Scanner(System.in);
        Funcionario novo = new Funcionario();

        for(int i=0; i < 50; i++){ //Gambiarra pra limpar a tela
            System.out.println();
        }

        System.out.println("[1] [Novo Funcionario]   Empresa do Balakobako ");
        System.out.println("-----------------------------------------------");
        System.out.print("Nome: ");
        novo.setNome(teclado.nextLine());
        System.out.print("Email: ");
        novo.setEmail( teclado.nextLine() );
        System.out.print("Salario: ");
        novo.setSalario( teclado.nextDouble());
        empresinha.contratar(novo);
        System.out.println(" Funcionario " + novo.getNome() +  " Cadastrado com Sucesso");
        System.out.println("*******************************************");
        System.out.println("Dados:");
        System.out.println("Nome: " + novo.getNome());
        System.out.println("Email: " + novo.getEmail());
        System.out.println("Salario: " + novo.getSalario());
    }

    public void executar (int opcao){
        switch(opcao){
            case 1:
                mostrarTelaIncluirFuncionario();
                break;
            case 2:
                mostrarTelaPedirAumento();
                break;
            case 3:
                empresinha.listarTelaDeFuncionario();
                break;
            case 4:
                empresinha.listarFuncionarioComMaiorSalario();
                break;
            case 5:
                empresinha.demitirFuncionairoComMaiorSalario();
                break;
            case 6:
                break;
            default:
                System.out.println("*********************************");
                System.out.println("*  404 error Opcao not found    *");
                System.out.println("*   Escolha uma Opcao valida    *");
                System.out.println("*********************************");
        }
    }

    private void mostrarTelaPedirAumento(){
        Scanner teclado = new Scanner(System.in);
        double aumento;
        do{
            System.out.println("Empresa do Balakobako");
            System.out.println("Digite o dissidio para o aumento de salario");
            aumento = teclado.nextDouble();
        }while(aumento >= 0 && aumento <= 1 );
        empresinha.concederAumento(aumento);
    }
/*
    public void listarTelaDeFuncionario(){
        Funcionario novo = new Funcionario();
        System.out.println();
        System.out.println("Nome: " + novo.getNome());
        System.out.println("Email: " + novo.getEmail());
        System.out.println("Salario: " + novo.getSalario());

    }*/

    public void mostrarTela(){
        Scanner teclado = new Scanner(System.in);
        int opcao = 0;
        do{  // Menu
            System.out.println();
            System.out.println("Empresa do Balakobako");
            System.out.println("1. Incluir Funcionario");
            System.out.println("2. Dar aumento");
            System.out.println("3. Listar Funcionarios");
            System.out.println("4. Funcionario Com Maior Salario");
            System.out.println("5. Demitir Funcionario Com Maior Salario");
            System.out.println("6. Sair");
            System.out.print("Opcao: ");
            opcao = teclado.nextInt();
            executar( opcao );
        }while(opcao != 6 );
    }

    public static void main(String[] args) {
        TelaEmpresa tela = new TelaEmpresa();
        tela.mostrarTela();
    }
}